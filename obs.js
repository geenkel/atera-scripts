var axios = require('axios')["default"];
var fs = require('fs');
var ini = require('ini');
var semver = require('semver');
var os = require("os");
var hostname = os.hostname();
var url = 'https://scheduler-rest.api.stg.livecontrol.io/api/v1/encoder_configuration/' + hostname;
//**EMS**
//EMS encoderIdServer.json
var encoderIdFileData = fs.readFileSync(process.env.USERPROFILE + '/EMS-multicam/encoderIdServer.json');
var encoderIdFile = JSON.parse(encoderIdFileData);
var cid = encoderIdFile.ST_ID || "";
var locationName = encoderIdFile.section_name || "";
//EMS EMS_init.json
var emsInitFileData = fs.readFileSync(process.env.USERPROFILE + '/EMS-multicam/EMS_init.json');
var emsInitFile = JSON.parse(emsInitFileData);
var emsApiRestUrl = emsInitFile.urlBase || "";
var emsApiWSurl = emsInitFile.wsUrl || "";
var emsVersion;
var emsVersionArray = emsInitFile.ems_version.split(' ');
for (var i = 0; i < emsVersionArray.length; i++) {
    if (emsVersionArray[i][0] == 'v') {
        emsVersion = semver.clean(emsVersionArray[i]);
    }
}
emsVersion = emsVersion || " ";
//**LCS**
//LCS ba_init.json
var baInitFileData = fs.readFileSync(process.env.USERPROFILE + '/AppData/Local/LiveControlStudioV3/BA_init.json');
var baInitFile = JSON.parse(baInitFileData);
var versionArray = baInitFile.lc_version.split(' ');
var lcsVersion;
for (var i = 0; i < versionArray.length; i++) {
    if (versionArray[i][0] == 'v') {
        lcsVersion = semver.clean(versionArray[i]);
    }
}
lcsVersion = lcsVersion || " ";
var lcsWorkspace = process.env.USERPROFILE + '/AppData/Local/LiveControlStudioV3/';
var lcsAppSettingsFileName;
var numberOfCameras = 0;
var is3_1 = false;
if (semver.valid(lcsVersion) && semver.ltr(lcsVersion, '3.1.0')) //version < 3.1
 {
    lcsAppSettingsFileName = lcsWorkspace + 'settingsTest.xml';
    numberOfCameras = 2;
}
else if (semver.valid(lcsVersion)) //version >= 3.1
 {
    lcsAppSettingsFileName = lcsWorkspace + 'appSettings.json';
    is3_1 = true;
    while (true) {
        try {
            var file = fs.statSync(lcsWorkspace + 'camera' + (numberOfCameras + 1).toString() + '/settings.json');
            numberOfCameras = numberOfCameras + 1;
        }
        catch (_a) {
            break;
        }
    }
}
console.log('Number of cameras = ' + numberOfCameras);
//LCS appSettings.json
var noVideFeed;
var audioSource;
var audioOffset;
var outputRecordingPath;
try {
    var appSettings = JSON.parse(fs.readFileSync(lcsAppSettingsFileName));
    noVideFeed = appSettings.noVideoFeed;
    audioSource = appSettings.obsAudioName || " ";
    audioOffset = appSettings.obsAudioSyncOffset || -1;
    outputRecordingPath = appSettings.obsFolder || " ";
}
catch (_b) {
    noVideFeed = false;
    audioSource = "";
    audioOffset = -1;
    outputRecordingPath = "";
}
//**OBS**
//OBS global.ini
var config = ini.parse(fs.readFileSync(process.env.APPDATA + '/obs-studio/global.ini', 'utf-8'));
var profileDir = config.Basic.ProfileDir;
var obsProfileName = config.Basic.Profile || "";
var sceneCollectionName = config.Basic.SceneCollection || "";
var virtualCameraAutoStart;
virtualCameraAutoStart = config.VirtualOutput.AutoStart;
var virtualCameraEnabled;
if (virtualCameraAutoStart != '') {
    virtualCameraEnabled = 'true';
}
else {
    virtualCameraEnabled = 'false';
    virtualCameraAutoStart = 'false';
}
//OBS Files names
var profileFileName = process.env.APPDATA + '/obs-studio/basic/profiles/' + profileDir + '/' + 'service.json';
var basicIniFileName = process.env.APPDATA + '/obs-studio/basic/profiles/' + profileDir + '/' + 'basic.ini';
var sceneCollectionFileName = process.env.APPDATA + '/obs-studio/basic/scenes/' + config.Basic.SceneCollectionFile + '.json';
var streamEncoderFileName = process.env.APPDATA + '/obs-studio/basic/profiles/' + profileDir + '/' + 'streamEncoder.json';
//OBS profile service.json
var obsStreamServer;
var obsStreamKey;
try {
    var obsProfileRawdata = fs.readFileSync(profileFileName);
    var obsData = JSON.parse(obsProfileRawdata);
    obsStreamServer = obsData.settings.server || " ";
    obsStreamKey = obsData.settings.key || " ";
}
catch (_c) {
    obsStreamKey = " ";
    obsStreamServer = " ";
}
//OBS profile basic.ini
var basicIni = ini.parse(fs.readFileSync(basicIniFileName, 'utf-8'));
var streamEncoder = basicIni.AdvOut.Encoder || "default";
var recEncoder = basicIni.AdvOut.RecEncoder || "default";
var outputResolution = basicIni.Video.OutputCX + 'x' + basicIni.Video.OutputCY || "default";
var canvasResolution = basicIni.Video.BaseCX + 'x' + basicIni.Video.BaseCY || "default";
var fps;
fps = basicIni.Video.FPSCommon || 30;
var outputMode = basicIni.Output.Mode || "default";
//OBS profile streamEncoder.json
var videoBitrate;
var encoderPreset;
try {
    var streamEncoderJson = JSON.parse(fs.readFileSync(streamEncoderFileName));
    videoBitrate = streamEncoderJson.bitrate || 2500;
    encoderPreset = streamEncoderJson.preset || "default";
}
catch (_d) {
    videoBitrate = 0;
    encoderPreset = " ";
}
try {
    var sceneCollectionJson = JSON.parse(fs.readFileSync(sceneCollectionFileName));
    var obsSources = sceneCollectionJson.sources;
    console.log(obsSources[0]);
    // for(let i = 0; i < numberOfCameras; i++)
    // {
    //   //find scene
    //   let sceneName: string
    //   if(is3_1){sceneName = 'Cam' + (i+1).toString()}
    //   else{sceneName = (i+1).toString()}
    //   for()
    // }  
}
catch (_e) {
}
var jsonBody = {
    'values': {
        'CID': +cid,
        'config.obs.stream.server': obsStreamServer,
        'config.obs.stream.stream_key': obsStreamKey,
        'config.obs.output.video_bitrate': +videoBitrate,
        'config.obs.output.video_encoder': streamEncoder,
        'config.obs.output.encoder_preset': encoderPreset,
        'config.obs.output.recording_encoder': recEncoder,
        'config.obs.video.base_resolution': canvasResolution,
        'config.obs.video.output_resolution': outputResolution,
        'config.obs.video.fps': +fps,
        'config.obs.virtual_camera.autostart': virtualCameraAutoStart.toString(),
        'config.obs.virtual_camera.enabled': virtualCameraEnabled.toString(),
        'config.obs.profile.name': obsProfileName,
        'config.obs.scene.collection_name': sceneCollectionName,
        'config.lcs.cameras.no_video_feed': noVideFeed.toString(),
        'config.lcs.audio.source': audioSource,
        'config.lcs.audio.offset': +audioOffset,
        'config.lcs.output.recording_path': outputRecordingPath,
        'config.ems.api.rest_url': emsApiRestUrl,
        'config.ems.api.ws_url': emsApiWSurl,
        'version.software.lcs': lcsVersion,
        'version.software.ems': emsVersion
    }
};
// console.log(jsonBody)
// axios.patch(url, jsonBody ,
//   {headers: {
//     'Content-Type': 'application/json' 
//   }})
//   .then(function (response) {
//     //console.log(response);
//   })
//   .catch(function (error) {
//     console.log(error);
//   });
