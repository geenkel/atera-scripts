const axios = require('axios').default;
const fs = require('fs');
const ini = require('ini');
var semver = require('semver');
var os = require("os");

var hostname = os.hostname();
let url = 'https://scheduler-rest.api.stg.livecontrol.io/api/v1/encoder_configuration/' + hostname

//**EMS**
//EMS encoderIdServer.json
let encoderIdFileData = fs.readFileSync(process.env.USERPROFILE + '/EMS-multicam/encoderIdServer.json');
let encoderIdFile = JSON.parse(encoderIdFileData);
let cid = encoderIdFile.ST_ID || ""
let locationName = encoderIdFile.section_name || ""
//EMS EMS_init.json
let emsInitFileData = fs.readFileSync(process.env.USERPROFILE + '/EMS-multicam/EMS_init.json');
let emsInitFile = JSON.parse(emsInitFileData);
let emsApiRestUrl = emsInitFile.urlBase || ""
let emsApiWSurl = emsInitFile.wsUrl || ""
let emsVersion :string  
let emsVersionArray = emsInitFile.ems_version.split(' ')
for (var i=0; i < emsVersionArray.length; i++) {
  if (emsVersionArray[i][0] == 'v'){ emsVersion = semver.clean(emsVersionArray[i])}
}
emsVersion = emsVersion || " "

//**LCS**
//LCS ba_init.json
let baInitFileData = fs.readFileSync(process.env.USERPROFILE + '/AppData/Local/LiveControlStudioV3/BA_init.json');
let baInitFile = JSON.parse(baInitFileData);
let versionArray = baInitFile.lc_version.split(' ')
let lcsVersion :string
for (var i=0; i < versionArray.length; i++) {
  if (versionArray[i][0] == 'v'){ lcsVersion = semver.clean(versionArray[i])}
}
lcsVersion = lcsVersion || " "

let lcsWorkspace = process.env.USERPROFILE + '/AppData/Local/LiveControlStudioV3/'
let lcsAppSettingsFileName: string
let numberOfCameras = 0
let is3_1 = false
if(semver.valid(lcsVersion) && semver.ltr(lcsVersion, '3.1.0') )   //version < 3.1
{
  lcsAppSettingsFileName = lcsWorkspace + 'settingsTest.xml'
  numberOfCameras = 2  
}
else if(semver.valid(lcsVersion))   //version >= 3.1
{
  lcsAppSettingsFileName = lcsWorkspace + 'appSettings.json'
  is3_1 = true
  while(true){
    try
    {
      let file = fs.statSync(lcsWorkspace + 'camera' + (numberOfCameras+1).toString() + '/settings.json');
      numberOfCameras = numberOfCameras + 1
    }catch{
      break;
    }
  }  
}

console.log('Number of cameras = ' + numberOfCameras)

//LCS appSettings.json
let noVideFeed : boolean
let audioSource : string
let audioOffset : number
let outputRecordingPath :string
try{
  let appSettings = JSON.parse(fs.readFileSync(lcsAppSettingsFileName)) 
  noVideFeed = appSettings.noVideoFeed
  audioSource = appSettings.obsAudioName || " "
  audioOffset = appSettings.obsAudioSyncOffset || -1
  outputRecordingPath = appSettings.obsFolder || " "
}catch
{
  noVideFeed =  false
  audioSource = ""
  audioOffset = -1
  outputRecordingPath = ""
}

//**OBS**
//OBS global.ini
const config = ini.parse(fs.readFileSync(process.env.APPDATA + '/obs-studio/global.ini', 'utf-8'));
var profileDir = config.Basic.ProfileDir;
let obsProfileName = config.Basic.Profile || ""
let sceneCollectionName = config.Basic.SceneCollection || ""

let virtualCameraAutoStart :string
virtualCameraAutoStart = config.VirtualOutput.AutoStart
let virtualCameraEnabled: string
if(virtualCameraAutoStart != ''){
  virtualCameraEnabled = 'true'
}
else{
  virtualCameraEnabled = 'false'
  virtualCameraAutoStart = 'false'
}
//OBS Files names
let profileFileName = process.env.APPDATA + '/obs-studio/basic/profiles/' + profileDir + '/' + 'service.json'
let basicIniFileName = process.env.APPDATA + '/obs-studio/basic/profiles/' + profileDir + '/' + 'basic.ini'
let sceneCollectionFileName = process.env.APPDATA + '/obs-studio/basic/scenes/' + config.Basic.SceneCollectionFile + '.json'
let streamEncoderFileName = process.env.APPDATA + '/obs-studio/basic/profiles/' + profileDir + '/' + 'streamEncoder.json'
//OBS profile service.json
let obsStreamServer: string
let obsStreamKey: string
try{
  let obsProfileRawdata = fs.readFileSync(profileFileName)
  let obsData = JSON.parse(obsProfileRawdata)
  obsStreamServer = obsData.settings.server || " "
  obsStreamKey = obsData.settings.key || " "
}catch{
  obsStreamKey = " "
  obsStreamServer = " "
}
//OBS profile basic.ini
let basicIni = ini.parse(fs.readFileSync(basicIniFileName, 'utf-8'));
let streamEncoder = basicIni.AdvOut.Encoder || "default"
let recEncoder = basicIni.AdvOut.RecEncoder || "default"
let outputResolution = basicIni.Video.OutputCX + 'x' +basicIni.Video.OutputCY || "default"
let canvasResolution = basicIni.Video.BaseCX + 'x' +basicIni.Video.BaseCY || "default"
let fps :number
fps = basicIni.Video.FPSCommon || 30
let outputMode = basicIni.Output.Mode || "default"
//OBS profile streamEncoder.json
let videoBitrate: number
let encoderPreset: string
try{
  let streamEncoderJson = JSON.parse(fs.readFileSync(streamEncoderFileName))
  videoBitrate = streamEncoderJson.bitrate || 2500
  encoderPreset = streamEncoderJson.preset || "default"
}
catch{
  videoBitrate = 0
  encoderPreset = " "
}

//OBS scenes
// interface ObsSource {
//   id: string
//   name: string  
// }
// interface SceneItem{
//   name: string
// }
// try{  
//   let sceneCollectionJson = JSON.parse(fs.readFileSync(sceneCollectionFileName))
//   let obsSources: Array<ObsSource> = sceneCollectionJson.sources  
//   for(let i = 0; i < numberOfCameras; i++)
//   {
//     //find scene
//     let sceneName: string
//     if(is3_1){sceneName = 'Cam' + (i+1).toString()}
//     else{sceneName = (i+1).toString()}
//     for(let source in obsSources)
//     {
//       if(source)
//     }
//   }  
// }catch{

// }

let jsonBody = {
  'values': {
    'CID' : +cid,
    'config.obs.stream.server' : obsStreamServer,
    'config.obs.stream.stream_key': obsStreamKey,
    'config.obs.output.video_bitrate' : +videoBitrate,
    'config.obs.output.video_encoder' : streamEncoder,  
    'config.obs.output.encoder_preset' : encoderPreset,
    'config.obs.output.recording_encoder' : recEncoder,
    'config.obs.video.base_resolution' : canvasResolution,
    'config.obs.video.output_resolution' : outputResolution,
    'config.obs.video.fps' : +fps,
    'config.obs.virtual_camera.autostart' : virtualCameraAutoStart.toString(),
    'config.obs.virtual_camera.enabled' : virtualCameraEnabled.toString(),
    'config.obs.profile.name' : obsProfileName,
    'config.obs.scene.collection_name' : sceneCollectionName,
    'config.lcs.cameras.no_video_feed' : noVideFeed.toString(),
    'config.lcs.audio.source' : audioSource,
    'config.lcs.audio.offset' : +audioOffset,
    'config.lcs.output.recording_path' : outputRecordingPath,
    'config.ems.api.rest_url' : emsApiRestUrl,
    'config.ems.api.ws_url' : emsApiWSurl,
    'version.software.lcs' : lcsVersion,
    'version.software.ems' : emsVersion
  }
}

console.log(jsonBody)

axios.patch(url, jsonBody ,
  {headers: {
    'Content-Type': 'application/json' 
  }})
  .then(function (response) {
    //console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });