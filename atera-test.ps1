$url = "https://livecontrol-desktop-updates-stg.s3.amazonaws.com/atera-scripts/script-test.zip"
Remove-Item $home/documents/atera-scripts/*.* -Recurse -Force
Get-ChildItem -Path $home/documents/atera-scripts/ -Recurse | Remove-Item -force -recurse
Remove-Item $home/documents/script.zip
Set-Location $home/documents
wget $url -outfile "script.zip"
Expand-Archive -LiteralPath $home/documents\script.zip -DestinationPath $home/documents/atera-scripts/
Set-Location $home/documents/atera-scripts
npm i
node obs.js